# TODO list

## UDP Commands
- [ ] Authing
 - [x] AUTH
 - [x] LOGOUT
 - [ ] ENCRYPT

- [ ] Notify
 - [ ] PUSH
 - [ ] NOTIFY
 - [ ] NOTIFYLIST
 - [ ] NOTIFYGET
 - [ ] NOTIFYACK

- [ ] Buddy
 - [ ] BUDDYADD
 - [ ] BUDDYDEL
 - [ ] BUDDYACCEPT
 - [ ] BUDDYDENY
 - [ ] BUDDYLIST
 - [ ] BUDDYSTATE

- [ ] Data
 - [x] ANIME
 - [ ] ANIMEDESC
 - [ ] CALENDAR
 - [ ] CHARACTER
 - [ ] CREATOR
 - [ ] EPISODE
 - [x] FILE
 - [ ] GROUP
 - [ ] GROUPSTATUS
 - [ ] UPDATED

- [ ] MyList
 - [ ] MYLIST
 - [ ] MYLISTADD
 - [ ] MYLISTDEL
 - [ ] MYLISTSTATS
 - [ ] VOTE
 - [ ] RANDOM

- [ ] Misc
 - [ ] MYLISTEXPORT
 - [x] PING
 - [x] VERSION
 - [x] UPTIME
 - [x] ENCODING
 - [ ] SENDMSG
 - [ ] USER

## HTTP Commands
- [ ] Data
 - [ ] Anime
 - [ ] Random Recommendations
 - [ ] Random Similar
 - [ ] Hot Anime
 - [ ] Main

- [ ] User Data
 - [ ] User MyList Summary
 - [ ] User Hints

