require 'spec_helper'
require 'anidb/version'

describe AniDB do
  it 'has a version number' do
    expect(AniDB::VERSION).not_to be nil
  end
end
