require 'yaml'

# AniDB API
module AniDB
    require_relative 'anidb/udp'
    require_relative 'anidb/html'

    CONFIG = YAML.load_file("#{File.dirname(__FILE__)}/config/anidb.yml")
    MASKS  = YAML.load_file("#{File.dirname(__FILE__)}/config/masks.yml")

    class << self
        def parse_attributes(attributes)
            attributes.reject! { |_key, value| value.nil? }
            attributes.map { |k, v| "#{k}=#{v}" }.join('&')
        end
    end
end
