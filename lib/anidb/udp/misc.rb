module AniDB
    class UDP
        module Misc
            def ping
                code, msg, _data = api_call(:ping, msg: 'nat=1', auth: false)

                case code
                when AniDB::PONG
                    "#{msg}"
                end
            end

            def version
                code, msg, _data = api_call(:version, auth: false)

                case code
                when AniDB::VERSION
                    "#{msg}"
                end
            end

            def uptime
                begin
                    code, msg, _data = api_call(:uptime)

                    case code
                    when AniDB::UPTIME
                        "#{msg}"
                    end
                rescue AniDB::NotLoggedInError
                    login
                    retry
                end
            end

            def encoding(name = 'ASCII')
                attr = parse_attributes({name: name})
                code, msg, _data = api_call(:encoding, msg: attr)

                case code
                when AniDB::ENCODING_CHANGED
                    "Encoding changed to #{name}."
                when AniDB::ENCODING_NOT_SUPPORTED
                    "Encoding #{name} is not supported."
                else
                    "#{msg}"
                end
            end

            def sendmsg
                # TODO: implement SEMDMSG command
            end

            def user
                # TODO: implement USER command
            end
        end
    end
end

