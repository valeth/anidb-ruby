require 'anidb/errors'

module AniDB
    class UDP
        # Authentification Commands
        module Auth
            def login(attributes = {})
                new_attributes = {
                    user:       @config[:username],
                    pass:       @config[:password],
                    protover:   AniDB::CONFIG[:protover],
                    client:     @config[:clientname],
                    clientver:  @config[:clientversion],
                    nat:        nil,      # default: 1
                    comp:       nil,      # default: 1
                    enc:        'utf-8',  # default: ASCII
                    mtu:        nil,
                    imgserver:  nil       # default: 1
                }
                new_attributes.update(attributes)

                [:user, :pass, :client, :clientver].each do |elem|
                    if new_attributes[elem].nil?
                        raise ArgumentError,
                        'Username, password, clientname and version required!'
                    end
                end

                attrs = AniDB.parse_attributes(new_attributes)

                code, msg, _data = api_call(:auth, msg: attrs, auth: false)

                case code
                when AniDB::LOGIN_ACCEPTED
                    @token = msg.split(' ').first
                when AniDB::LOGIN_ACCEPTED_NEW_VERSION
                    @token = msg.split(' ').first
                when AniDB::LOGIN_FAILED
                    raise AniDB::LoginError, msg
                when AniDB::CLIENT_VERSION_OUTDATED
                    raise AniDB::ClientVersionError, msg
                when AniDB::CLIENT_BANNED
                    raise AniDB::ClientBannedError, msg
                end
            end

            def logout
                begin
                    code, msg, _data = api_call(:logout)

                    case code
                    when AniDB::LOGGED_OUT
                        'LOGGED OUT'
                    when AniDB::NOT_LOGGED_IN
                        raise NotLoggedInError, msg
                    end
                rescue NotLoggedInError => e
                    puts e.message
                end

                @token = nil
            end

            def encrypt
                # TODO: implement ENCRYPT command
            end

            def authenticated?
                !@token.nil?
            end

            def auth_token
                @token
            end
        end
    end
end
