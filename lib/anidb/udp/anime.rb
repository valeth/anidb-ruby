require 'digest/ed2k'

module AniDB
    class UDP
        # Anime Commands
        module Anime
            # options: aid, [amask]
            #          aname, [amask]
            # returns: 230 ANIME
            #          aid|ep_count|special_count|rating|temp_rating|temp_votes|review_rating_average|reviews|year|type|romaji_name|kanji_name|english_name|other_name|short_names_list|synonyms_list|category_list
            #
            #          230 ANIME
            #          {ordered amask}
            #
            #          330 NO SUCH ANIME
            def anime(attributes = {})
                amask = attributes[:amask] || AniDB::MASKS[:anime][:default_amask]
                amask = order_mask(:anime, :amasks, amask)
                attributes[:amask] = parse_mask(:anime, :amasks, 14, amask)

                # aid OR aname
                if attributes.include?(:aid)
                    attributes.delete(:aname)
                elsif attributes.include?(:aname)
                    attributes.delete(:aid)
                end

                attrs = AniDB.parse_attributes(attributes)

                begin
                    code, msg, data = api_call(:anime, msg: attrs)

                    case code
                    when AniDB::ANIME
                        data = data.map { |e| e.force_encoding('utf-8') }
                        tmp = amask.zip(data)
                        tmp = tmp.map { |e| [e[0], e[1].nil? ? '' : e[1]] }
                        tmp.to_h
                    when AniDB::NO_SUCH_ANIME
                        raise AniDB::NotFoundError, msg
                    end
                rescue AuthError
                    login
                    retry
                end
            end

            # options: aid, part
            # returns: 233 ANIMEDESC
            #          cur_part|max_parts|description
            def animedesc(attributes = {})
                if attributes[:aid].nil? || attributes[:part].nil?
                    raise ArgumentError, 'aid and part options required'
                end

                attrs = parse_attributes(attributes)

                begin
                    code, msg, data = api_call(:animedesc, msg: attrs)
                    [:cur_part, :max_parts, :description].zip(data)
                    # TODO: fetch multiple parts in necessary

                    case code
                    when AniDB::ANIMEDESC
                    when AniDB::NO_SUCH_ANIME
                        raise AniDB::NotFoundError, msg
                    when AniDB::NO_SUCH_DESCRIPTION
                        raise AniDB::NotFoundError, msg
                    end
                rescue AuthError
                    login
                    retry
                end
            end

            def calendar(attributes = {})
                raise 'not implemented!'

                begin
                    code, msg, data = api_call(:calendar, msg: attrs)

                    case code
                    when AniDB::CALENDAR
                    when AniDB::CALENDAR_EMPTY
                    end
                rescue AuthError
                    login
                    retry
                end
            end

            def character(attributes = {})
                raise 'not implemented!'

                begin
                    code, msg, data = api_call(:character, msg: attrs)
                rescue AuthError
                    login
                    retry
                end
            end

            def creator(attributes = {})
                raise 'not implemented!'

                begin
                    code, msg, data = api_call(:creator, msg: attrs)
                rescue AuthError
                    login
                    retry
                end
            end

            # options: eid
            #          aname, epno
            #          aid, epno
            # returns: 240 EPISODE
            #          340 NO SUCH EPISODE
            def episode(attributes = {})
                raise 'not implemented!'

                begin
                    code, msg, data = api_call(:episode, msg: attrs)
                end
            end

            def file(filename = nil, attributes = {})
                fmask = attributes[:fmask] || AniDB::MASKS[:file][:default_fmask]
                amask = attributes[:amask] || AniDB::MASKS[:file][:default_amask]

                fmask = order_mask(:file, :fmask, fmask)
                amask = order_mask(:file, :amask, amask)

                attributes[:fmask] = parse_mask(:file, :fmasks, 10, fmask)
                attributes[:amask] = parse_mask(:file, :amasks, 8, amask)

                unless filename.nil?
                    if attributes[:ed2k].nil?
                        attributes[:ed2k] = Digest::ED2K.file(filename).hexdigest
                    end

                    attributes[:size] = File.size(filename)
                end

                attrs = AniDB.parse_attributes(attributes)

                begin
                    code, msg, data = api_call(:file, msg: attrs)

                    case code
                    when AniDB::FILE
                        data = data.map { |elem| elem.force_encoding('utf-8') }
                        tmp = ([:fid] + fmask + amask).zip(data)
                        tmp = tmp.map { |e| [e[0], e[1].nil? ? '' : e[1]] }
                        tmp.to_h
                    when AniDB::MULTIPLE_FILES_FOUND
                        data
                    when AniDB::NO_SUCH_FILE
                        raise AniDB::NotFoundError, msg
                    end
                rescue AniDB::AuthError
                    login
                    retry
                end
            end

            def group
                raise 'not implemented!'
            end

            def groupstatus
                raise 'not implemented!'
            end

            def updated
                raise 'not implemented!'
            end

            private

            def order_mask(cmd, mask, values)
                unless values.empty?
                    AniDB::MASKS[cmd][mask].keys.select do |key|
                        values.include?(key)
                    end
                else
                    []
                end
            end

            def parse_mask(cmd, mask, hexlength, values)
                unless values.empty?
                    val = values.inject(0) do |m, e|
                        m + AniDB::MASKS[cmd.downcase][mask.downcase][e]
                    end

                    format("%0#{hexlength}x", val)
                end
            end
        end
    end
end
