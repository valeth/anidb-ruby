require 'socket'
require 'timeout'

require 'anidb'
require_relative 'errors'
require_relative 'response'
require_relative 'udp/anime'
require_relative 'udp/auth'
require_relative 'udp/buddy'
require_relative 'udp/misc'
require_relative 'udp/mylist'
require_relative 'udp/notify'

module AniDB
    def self.UDP(attributes = {})
        AniDB::UDP.new(attributes)
    end

    # AniDB UDP client
    class UDP
        include AniDB::UDP::Anime
        include AniDB::UDP::Auth
        include AniDB::UDP::Buddy
        include AniDB::UDP::Misc
        include AniDB::UDP::MyList
        include AniDB::UDP::Notify

        attr_reader :connected
        attr_reader :configured

        def initialize(attributes = {})
            @connected = false
            @activity = Time.now

            configure(attributes)
            connect
        end

        def connect(attributes = {})
            unless @connected
                localport = attributes[:localport] || AniDB::CONFIG[:localport]
                url = attributes[:url] || AniDB::CONFIG[:url]
                port = attributes[:port] || AniDB::CONFIG[:port]
                begin
                    @sock = UDPSocket.new
                    @sock.bind(0, localport)
                    @sock.connect(url, port)
                    @connected = true
                    nil
                rescue Errno::EADDRINUSE => e
                    e.message
                end
            end
        end

        def disconnect
            begin
                if @connected
                    @sock.shutdown
                    @sock.close
                end
                @connected = false
                nil
            rescue Errno::EADDRINUSE => e
                e.message
            end
        end

        def configure(attributes = {})
            @configured = false
            keys = [:clientname, :clientversion, :username, :password]

            if (keys & attributes.keys).size == keys.size
                @config = attributes
                @configured = true
            elsif attributes[:path]
                @config = YAML.load_file(attributes[:path])
                @configured = true
            end
        end

        def inspect
            "#<UDP Client Connection:  connected=#{@connected}, configured=#{@configured}, last activity=#{@activity}>"
        end

        private

        def keepalive
            uptime if @connected && (Time.now - @activity) > 120
        end

        def api_call(cmd, attributes = {})
            raise ArgumentError, 'Load client configuration first' unless @configured
            diff = (Time.now - @activity)
            sleep(4 - diff) if diff < 4

            msg = attributes[:msg] || ''

            unless attributes[:auth] == false
                token = AniDB.parse_attributes(s: @token)
                msg += "&#{token}" unless token.empty?
            end

            puts "#{cmd.upcase} #{msg}"
            send_message("#{cmd.upcase} #{msg}")
        end

        def send_message(msg)
            retries = [30, 120, 300, 600, 1800, 3600, 7200]
            begin
                Timeout.timeout(30) do
                    @sock.send(msg, 0)
                    recv, _addr = @sock.recvfrom(2048)
                    @activity = Time.now

                    response, data = recv.split("\n")
                    unless data.nil?
                        data.force_encoding('utf-8')
                        data = data.split('|')
                    end
                    code, *msg = response.split(' ')
                    msg = msg.join(' ') unless msg.nil?

                    handle_errors(code, msg, data)
                    [code, msg, data]
                end
            rescue Timeout::Error
                wait_time = retries.shift
                raise Timeout::Error if wait_time.nil?
                puts "Waiting #{wait_time} seconds..."
                sleep(wait_time)
                retry
            end
        end

        def handle_errors(code, msg, data)
            case code
            when AniDB::ILLEGAL_INPUT_OR_ACCESS_DENIED
                raise AniDB::IllegalInputError, msg
            when AniDB::BANNED
                data = data.join(' ') unless data.nil?
                raise AniDB::BannedError, "#{msg}: #{data}"
            when AniDB::UNKNOWN_COMMAND
                raise AniDB::UnknownCommandError, msg
            when AniDB::INTERNAL_SERVER_ERROR
                raise AniDB::InternalServerError, msg
            when AniDB::ANIDB_OUT_OF_SERVICE
                raise AniDB::AniDBOutOfServiceError, msg
            when AniDB::SERVER_BUSY
                raise AniDB::ServerBusyError, msg
            when AniDB::TIMEOUT
                raise AniDB::TimeoutError, msg
            when AniDB::LOGIN_FIRST
                raise AniDB::NotLoggedInError, msg
            when AniDB::ACCESS_DENIED
                raise AniDB::AccessDeniedError, msg
            when AniDB::INVALID_SESSION
                raise AniDB::InvalidSessionError, msg
            end
        end
    end
end
