module AniDB
    class IllegalInputError < RuntimeError; end
    class BannedError < RuntimeError; end
    class UnknownCommandError < RuntimeError; end
    class InternalServerError < RuntimeError; end
    class AniDBOutOfServiceError < RuntimeError; end
    class ServerBusyError < RuntimeError; end
    class TimeoutError < RuntimeError; end

    class ClientVersionError < RuntimeError; end
    class ClientBannedError < RuntimeError; end

    class AuthError < RuntimeError; end
    class NotLoggedInError < AuthError; end
    class AccessDeniedError < AuthError; end
    class InvalidSessionError < AuthError; end

    class NotFoundError < RuntimeError; end
end
